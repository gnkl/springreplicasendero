/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_movimientos_sendero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMovimientosSendero.findAll", query = "SELECT t FROM TbMovimientosSendero t"),
    @NamedQuery(name = "TbMovimientosSendero.findByFClave", query = "SELECT t FROM TbMovimientosSendero t WHERE t.fClave = :fClave"),
    @NamedQuery(name = "TbMovimientosSendero.findByFSolicitado", query = "SELECT t FROM TbMovimientosSendero t WHERE t.fSolicitado = :fSolicitado"),
    @NamedQuery(name = "TbMovimientosSendero.findByFSurtido", query = "SELECT t FROM TbMovimientosSendero t WHERE t.fSurtido = :fSurtido"),
    @NamedQuery(name = "TbMovimientosSendero.findByFFechaFactura", query = "SELECT t FROM TbMovimientosSendero t WHERE t.fFechaFactura = :fFechaFactura"),
    @NamedQuery(name = "TbMovimientosSendero.findByFFecActua", query = "SELECT t FROM TbMovimientosSendero t WHERE t.fFecActua = :fFecActua")})
public class TbMovimientosSendero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "F_Clave")
    private String fClave;
    @Basic(optional = false)
    @Column(name = "F_Solicitado")
    private int fSolicitado;
    @Basic(optional = false)
    @Column(name = "F_Surtido")
    private int fSurtido;
    @Column(name = "FechaFactura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFactura;
    @Basic(optional = false)
    @Column(name = "FecActua")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecActua;

    public TbMovimientosSendero() {
    }

    public TbMovimientosSendero(String fClave) {
        this.fClave = fClave;
    }

    public TbMovimientosSendero(String fClave, int fSolicitado, int fSurtido, Date fFecActua) {
        this.fClave = fClave;
        this.fSolicitado = fSolicitado;
        this.fSurtido = fSurtido;
        this.fecActua = fFecActua;
    }

    public String getFClave() {
        return fClave;
    }

    public void setFClave(String fClave) {
        this.fClave = fClave;
    }

    public int getFSolicitado() {
        return fSolicitado;
    }

    public void setFSolicitado(int fSolicitado) {
        this.fSolicitado = fSolicitado;
    }

    public int getFSurtido() {
        return fSurtido;
    }

    public void setFSurtido(int fSurtido) {
        this.fSurtido = fSurtido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fClave != null ? fClave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMovimientosSendero)) {
            return false;
        }
        TbMovimientosSendero other = (TbMovimientosSendero) object;
        if ((this.fClave == null && other.fClave != null) || (this.fClave != null && !this.fClave.equals(other.fClave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.replica.model.TbMovimientosSendero[ fClave=" + fClave + " ]";
    }

    /**
     * @return the fechaFactura
     */
    public Date getFechaFactura() {
        return fechaFactura;
    }

    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    /**
     * @return the fecActua
     */
    public Date getFecActua() {
        return fecActua;
    }

    /**
     * @param fecActua the fecActua to set
     */
    public void setFecActua(Date fecActua) {
        this.fecActua = fecActua;
    }
    
}
