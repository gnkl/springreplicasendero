/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.bean;

import java.util.Date;

/**
 *
 * @author jmejia
 */
public class SenderoData {

    private String clapro;
    private String clalot;
    private Date feccad;
    private Long sum;

    /**
     * @return the clapro
     */
    public String getClapro() {
        return clapro;
    }

    /**
     * @param clapro the clapro to set
     */
    public void setClapro(String clapro) {
        this.clapro = clapro;
    }

    /**
     * @return the clalot
     */
    public String getClalot() {
        return clalot;
    }

    /**
     * @param clalot the clalot to set
     */
    public void setClalot(String clalot) {
        this.clalot = clalot;
    }

    /**
     * @return the feccad
     */
    public Date getFeccad() {
        return feccad;
    }

    /**
     * @param feccad the feccad to set
     */
    public void setFeccad(Date feccad) {
        this.feccad = feccad;
    }

    /**
     * @return the sum
     */
    public Long getSum() {
        return sum;
    }

    /**
     * @param sum the sum to set
     */
    public void setSum(Long sum) {
        this.sum = sum;
    }
    
    
}
