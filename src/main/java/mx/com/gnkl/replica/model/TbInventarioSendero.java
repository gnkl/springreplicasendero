/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_inventario_sendero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbInventarioSendero.findAll", query = "SELECT t FROM TbInventarioSendero t"),
    @NamedQuery(name = "TbInventarioSendero.findByFClave", query = "SELECT t FROM TbInventarioSendero t WHERE t.fClave = :fClave"),
    @NamedQuery(name = "TbInventarioSendero.findByFLote", query = "SELECT t FROM TbInventarioSendero t WHERE t.fLote = :fLote"),
    @NamedQuery(name = "TbInventarioSendero.findByFCaducidad", query = "SELECT t FROM TbInventarioSendero t WHERE t.fCaducidad = :fCaducidad"),
    @NamedQuery(name = "TbInventarioSendero.findByFExistencia", query = "SELECT t FROM TbInventarioSendero t WHERE t.fExistencia = :fExistencia"),
    @NamedQuery(name = "TbInventarioSendero.findByFFecActua", query = "SELECT t FROM TbInventarioSendero t WHERE t.fFecActua = :fFecActua"),
    @NamedQuery(name = "TbInventarioSendero.findByFId", query = "SELECT t FROM TbInventarioSendero t WHERE t.fId = :fId")})
public class TbInventarioSendero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "F_Clave")
    private String fClave;
    @Basic(optional = false)
    @Column(name = "F_Lote")
    private String fLote;
    @Basic(optional = false)
    @Column(name = "F_Caducidad")
    @Temporal(TemporalType.DATE)
    private Date fCaducidad;
    @Basic(optional = false)
    @Column(name = "F_Existencia")
    private int fExistencia;
    @Basic(optional = false)
    @Column(name = "F_FecActua")
    @Temporal(TemporalType.DATE)
    private Date fFecActua;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "F_Id")
    private Integer fId;

    public TbInventarioSendero() {
    }

    public TbInventarioSendero(Integer fId) {
        this.fId = fId;
    }

    public TbInventarioSendero(Integer fId, String fClave, String fLote, Date fCaducidad, int fExistencia, Date fFecActua) {
        this.fId = fId;
        this.fClave = fClave;
        this.fLote = fLote;
        this.fCaducidad = fCaducidad;
        this.fExistencia = fExistencia;
        this.fFecActua = fFecActua;
    }

    public String getFClave() {
        return fClave;
    }

    public void setFClave(String fClave) {
        this.fClave = fClave;
    }

    public String getFLote() {
        return fLote;
    }

    public void setFLote(String fLote) {
        this.fLote = fLote;
    }

    public Date getFCaducidad() {
        return fCaducidad;
    }

    public void setFCaducidad(Date fCaducidad) {
        this.fCaducidad = fCaducidad;
    }

    public int getFExistencia() {
        return fExistencia;
    }

    public void setFExistencia(int fExistencia) {
        this.fExistencia = fExistencia;
    }

    public Date getFFecActua() {
        return fFecActua;
    }

    public void setFFecActua(Date fFecActua) {
        this.fFecActua = fFecActua;
    }

    public Integer getFId() {
        return fId;
    }

    public void setFId(Integer fId) {
        this.fId = fId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fId != null ? fId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbInventarioSendero)) {
            return false;
        }
        TbInventarioSendero other = (TbInventarioSendero) object;
        if ((this.fId == null && other.fId != null) || (this.fId != null && !this.fId.equals(other.fId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.replica.model.TbInventarioSendero[ fId=" + fId + " ]";
    }
    
}
