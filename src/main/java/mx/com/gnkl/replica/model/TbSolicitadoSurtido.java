/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jmejia
 */
public class TbSolicitadoSurtido implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @Column(name = "clapro")
    private String clapro;
    @Basic(optional = false)
    @Column(name = "fecent")
    @Temporal(TemporalType.DATE)
    private Date fecent;
    @Basic(optional = false)
    @Column(name = "requerido")
    private int requerido;
    @Basic(optional = false)
    @Column(name = "surtido")
    private int surtido;

    public TbSolicitadoSurtido() {
    }


    public TbSolicitadoSurtido(String clapro, Date fecent, int requerido, int surtido) {
        this.clapro=clapro;
        this.fecent=fecent;
        this.requerido = requerido;
        this.surtido = surtido;
    }

    public int getRequerido() {
        return requerido;
    }

    public void setRequerido(int requerido) {
        this.requerido = requerido;
    }

    public int getSurtido() {
        return surtido;
    }

    public void setSurtido(int surtido) {
        this.surtido = surtido;
    }

    /**
     * @return the clapro
     */
    public String getClapro() {
        return clapro;
    }

    /**
     * @param clapro the clapro to set
     */
    public void setClapro(String clapro) {
        this.clapro = clapro;
    }

    /**
     * @return the fecent
     */
    public Date getFecent() {
        return fecent;
    }

    /**
     * @param fecent the fecent to set
     */
    public void setFecent(Date fecent) {
        this.fecent = fecent;
    }

}
