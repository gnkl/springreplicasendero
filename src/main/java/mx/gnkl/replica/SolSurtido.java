/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.replica;

import java.util.Date;
import java.util.List;
import mx.com.gnkl.replica.model.TbSolicitadoSurtido;
import mx.com.gnkl.replica.repository.ReplicaDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author jmejia
 */
public class SolSurtido {
    
    public static void main(String[] args)throws Exception{
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+new Date());
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("appconfig-data.xml");
        //ReplicaDao replicaDao = (ReplicaDao) ctx.getBean("transferTransactionTemplate");
        final ReplicaDao replicaDao = (ReplicaDao) ctx.getBean(ReplicaDao.class);
        try{
            List<TbSolicitadoSurtido> list = replicaDao.getTbTbSolicitadoSurtidoFromSendero();
            replicaDao.persistSolSurtDataToLocalDb(list);            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+new Date());
    }    
    
}
