/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.replica;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.com.gnkl.replica.bean.SenderoData;
import mx.com.gnkl.replica.model.TbInventarioSendero;
import mx.com.gnkl.replica.model.TbMovimientosSendero;
import mx.com.gnkl.replica.repository.ReplicaDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author jmejia
 */
public class Principal {
    
    public static void main(String[] args)throws Exception{
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+new Date());
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("appconfig-data.xml");
        //ReplicaDao replicaDao = (ReplicaDao) ctx.getBean("transferTransactionTemplate");
        final ReplicaDao replicaDao = (ReplicaDao) ctx.getBean(ReplicaDao.class);

        try {
            List<TbInventarioSendero> list = replicaDao.getTbInventarioSenderoFromSendero();
            replicaDao.persistDataToLocalDb(list);
        } catch (Exception ex) {
            System.err.println("Exception inventario "+ex.getLocalizedMessage());
        }

        try {
            List<TbMovimientosSendero> listMovimiento = replicaDao.getTbMovimientosSenderoFromSendero();
            replicaDao.persistMovimientoDataToLocalDb(listMovimiento);
        } catch (Exception ex) {
            System.err.println("Exception movimientos "+ex.getLocalizedMessage());
        }
        
        ((ClassPathXmlApplicationContext) (ctx)).close();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+new Date());        
    }
    
}
