/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.repository.impl;


import com.google.common.collect.Lists;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import mx.com.gnkl.replica.bean.SenderoData;
import mx.com.gnkl.replica.model.TbInventarioSendero;
import mx.com.gnkl.replica.model.TbMovimientosSendero;
import mx.com.gnkl.replica.model.TbSolicitadoSurtido;
import mx.com.gnkl.replica.repository.ReplicaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jmejia
 */
@Component
public class ReplicaDaoImpl implements ReplicaDao{

    @Autowired
    private JdbcTemplate jdbcTemplateLerma;

    @Autowired
    private JdbcTemplate jdbcTemplateSendero;

    @Override
    public Integer testSenderoConnection() {
        return this.jdbcTemplateSendero.queryForObject("SELECT 1 FROM DUAL ", Integer.class);    
    }

    @Override
    public Integer testLermaConnection() {
        return this.jdbcTemplateLerma.queryForObject("SELECT 1 FROM DUAL ", Integer.class);  
    }

    @Override
    public List<SenderoData> getDataFromSendero() {
        String senderoQuery = "SELECT F_ClaPro as clapro,F_ClaLot as clalot,F_FecCad as feccad,SUM(F_ExiLot) as sum FROM tb_lote WHERE F_ExiLot!=0 GROUP BY F_FolLot ORDER BY F_ClaPro";
        return this.jdbcTemplateSendero.query(senderoQuery, new BeanPropertyRowMapper(SenderoData.class));    
    }

    @Override
    public List<TbInventarioSendero> getTbInventarioSenderoFromSendero() {
        //String senderoQuery = "SELECT F_ClaPro AS F_Clave,F_ClaLot AS F_Lote,F_FecCad AS F_Caducidad,SUM(F_ExiLot) AS F_Existencia , NOW() AS F_FecActua FROM tb_lote WHERE F_ExiLot!=0 GROUP BY F_FolLot ORDER BY F_ClaPro";
        String senderoQuery = "SELECT F_ClaPro AS F_Clave,F_ClaLot AS F_Lote,F_FecCad AS F_Caducidad,SUM(F_ExiLot) AS F_Existencia , NOW() AS F_FecActua FROM tb_lote WHERE F_ExiLot!=0 GROUP BY F_FolLot ORDER BY F_ClaPro";
        return this.jdbcTemplateSendero.query(senderoQuery, new BeanPropertyRowMapper(TbInventarioSendero.class));    
    }

    @Override
    public List<TbMovimientosSendero> getTbMovimientosSenderoFromSendero() {
        String senderoQuery = "SELECT F_ClaPro AS F_Clave,SUM(F_CantReq) AS F_Solicitado,SUM(F_CantSur) AS F_Surtido, MAX(F_FecEnt) AS FechaFactura, CURDATE() AS FecActua FROM tb_factura WHERE F_FecEnt<=CURDATE() AND F_StsFact='A' GROUP BY F_ClaPro";
        return this.jdbcTemplateSendero.query(senderoQuery, new BeanPropertyRowMapper(TbMovimientosSendero.class));    
    }
    
    @Override
    public List<TbSolicitadoSurtido> getTbTbSolicitadoSurtidoFromSendero() {
        String senderoQuery = "SELECT F_ClaPro AS clapro, F_FecEnt AS fecent, sum(F_CantReq) requerido, sum(F_CantSur) surtido FROM tb_factura WHERE F_FecEnt=(CURDATE() - INTERVAL 1 day) GROUP BY F_ClaPro";
        return this.jdbcTemplateSendero.query(senderoQuery, new BeanPropertyRowMapper(TbSolicitadoSurtido.class));  
    }    

    @Override
    public List<TbSolicitadoSurtido> getTbSolicitadoSurtido1YearFromSendero() {
        String senderoQuery = "SELECT F_ClaPro AS clapro, F_FecEnt AS fecent, sum(F_CantReq) Requerido, sum(F_CantSur) surtido FROM tb_factura GROUP BY F_ClaPro ORDER BY F_FecEnt DESC";
        return this.jdbcTemplateSendero.query(senderoQuery, new BeanPropertyRowMapper(TbSolicitadoSurtido.class));  
    }    
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void persistDataToLocalDb(final List<TbInventarioSendero> list){
        final int batchSize = 500;
        if(list!=null && !list.isEmpty()){
            deleteTableInventarioData();
            List<List<TbInventarioSendero>> batchLists = Lists.partition(list, batchSize);
            for (final List<TbInventarioSendero> batch : batchLists) {
                final String inserQuery = "insert into tb_inventario_sendero (F_Clave, F_Lote, F_Caducidad, F_Existencia, F_FecActua) values (?, ?, ?, ?, ?) ";
                jdbcTemplateLerma.batchUpdate(inserQuery, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        TbInventarioSendero objinv = batch.get(i);
                        //ps.setInt(1, Integer.valueOf(objinv.getFClave()));
                        ps.setString(1, objinv.getFClave());
                        ps.setString(2, objinv.getFLote());
                        ps.setDate(3, new java.sql.Date(objinv.getFCaducidad().getTime()));
                        ps.setInt(4, objinv.getFExistencia());
                        Date current = new Date();
                        ps.setDate(5, new java.sql.Date(current.getTime()));
                    }

                    @Override
                    public int getBatchSize() {
                        return batch.size();
                    }
                });
                //throw new Exception("test ");
            }
            
        }
        
    }
    
    private void deleteTableInventarioData(){
        String senderoQuery = "DELETE FROM tb_inventario_sendero";
        this.jdbcTemplateLerma.update(senderoQuery);
    }

    private void deleteTableMovimientoData(){
        String senderoQuery = "DELETE FROM tb_movimientos_sendero";
        this.jdbcTemplateLerma.update(senderoQuery);
    }

    private void deleteSolVsSurtidoData(){
        String senderoQuery = "DELETE FROM tb_solicitado_surtido WHERE fechaact=CURDATE()";
        this.jdbcTemplateLerma.update(senderoQuery);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void persistMovimientoDataToLocalDb(List<TbMovimientosSendero> list) throws Exception {
        final int batchSize = 500;
        if(list!=null && !list.isEmpty()){
            deleteTableMovimientoData();
            List<List<TbMovimientosSendero>> batchLists = Lists.partition(list, batchSize);

            for (final List<TbMovimientosSendero> batch : batchLists) {
                final String inserQuery = "insert into tb_movimientos_sendero (F_Clave, F_Solicitado, F_Surtido, F_FechaFactura, F_FecActua) values (?, ?, ?, ?, ?) ";
                jdbcTemplateLerma.batchUpdate(inserQuery, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        TbMovimientosSendero objmov = batch.get(i);
                        //ps.setInt(1, Integer.valueOf(objmov.getFClave()));
                        ps.setString(1, objmov.getFClave());
                        ps.setInt(2, objmov.getFSolicitado());
                        ps.setInt(3, objmov.getFSurtido());
                        ps.setDate(4, new java.sql.Date(objmov.getFechaFactura().getTime()));
                        Date current = new Date();
                        ps.setDate(5, new java.sql.Date(current.getTime()));
                    }

                    @Override
                    public int getBatchSize() {
                        return batch.size();
                    }
                });
            }
            
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void persistSolSurtDataToLocalDb(List<TbSolicitadoSurtido> list) throws Exception {
        final int batchSize = 500;
        if(list!=null && !list.isEmpty()){
            this.jdbcTemplateLerma.update("DELETE FROM tb_solicitado_surtido WHERE fechaact=CURDATE()");
            List<List<TbSolicitadoSurtido>> batchLists = Lists.partition(list, batchSize);

            for (final List<TbSolicitadoSurtido> batch : batchLists) {
                final String inserQuery = "insert into tb_solicitado_surtido (clapro, fecent, requerido, surtido, fechaact) values (?, ?, ?, ?, CURDATE()) ";
                jdbcTemplateLerma.batchUpdate(inserQuery, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        TbSolicitadoSurtido objsolsurt = batch.get(i);
                        //ps.setInt(1, Integer.valueOf(objmov.getFClave()));
                        ps.setString(1, objsolsurt.getClapro());
                        ps.setDate(2, new java.sql.Date(objsolsurt.getFecent().getTime()));
                        ps.setInt(3, objsolsurt.getRequerido());
                        ps.setInt(4, objsolsurt.getSurtido());
                        //Date current = new Date();
                        //ps.setDate(5, new java.sql.Date(current.getTime()));
                    }

                    @Override
                    public int getBatchSize() {
                        return batch.size();
                    }
                });
            }
            
        }

    }    
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void persistSolSurt1YearDataToLocalDb(List<TbSolicitadoSurtido> list) throws Exception {
        final int batchSize = 500;
        if(list!=null && !list.isEmpty()){
            this.jdbcTemplateLerma.update("DELETE FROM tb_solicitado_surtido");
            List<List<TbSolicitadoSurtido>> batchLists = Lists.partition(list, batchSize);

            for (final List<TbSolicitadoSurtido> batch : batchLists) {
                final String inserQuery = "insert into tb_solicitado_surtido (clapro, fecent, requerido, surtido, fechaact) values (?, ?, ?, ?, CURDATE()) ";
                jdbcTemplateLerma.batchUpdate(inserQuery, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        TbSolicitadoSurtido objsolsurt = batch.get(i);
                        //ps.setInt(1, Integer.valueOf(objmov.getFClave()));
                        ps.setString(1, objsolsurt.getClapro());
                        ps.setDate(2, new java.sql.Date(objsolsurt.getFecent().getTime()));
                        ps.setInt(3, objsolsurt.getRequerido());
                        ps.setInt(4, objsolsurt.getSurtido());
                        //Date current = new Date();
                        //ps.setDate(5, new java.sql.Date(current.getTime()));
                    }

                    @Override
                    public int getBatchSize() {
                        return batch.size();
                    }
                });
            }
            
        }

    }        
}
