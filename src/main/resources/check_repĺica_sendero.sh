#!/bin/sh

PATH_JAR=/home/jmejia/Documents/Workspace/SpringReplicaSendero/target/

JAR_NAME_REPLICA=SpringReplicaSendero-1.0.jar


echo $PATH_JAR$JAR_NAME_REPLICA;
echo "SEARCH FOR JAR EXECUTION " $JAR_NAME_REPLICA
PID_JAR=$(ps -fea | grep $JAR_NAME_REPLICA | grep -v grep | awk '{print $2}')	
echo "$PID_JAR";
if [ -z "$PID_JAR" ]; then
	echo "NO PROCESS JAR FOUND";
	echo "TRY TO EXECUTE PROCESS";
	java -jar $PATH_JAR$JAR_NAME_REPLICA &
else
	echo "PROCESS IS RUNNING."	
fi
