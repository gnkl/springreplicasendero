/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.repository;

import java.util.List;
import mx.com.gnkl.replica.bean.SenderoData;
import mx.com.gnkl.replica.model.TbInventarioSendero;
import mx.com.gnkl.replica.model.TbMovimientosSendero;
import mx.com.gnkl.replica.model.TbSolicitadoSurtido;

/**
 *
 * @author jmejia
 */
public interface ReplicaDao {
    
    public Integer testSenderoConnection();
    public Integer testLermaConnection();
    public List<SenderoData> getDataFromSendero();
    public List<TbInventarioSendero> getTbInventarioSenderoFromSendero();
    public List<TbMovimientosSendero> getTbMovimientosSenderoFromSendero();
    public List<TbSolicitadoSurtido> getTbTbSolicitadoSurtidoFromSendero();
    public void persistDataToLocalDb(final List<TbInventarioSendero> list) throws Exception;
    public void persistMovimientoDataToLocalDb(final List<TbMovimientosSendero> list)throws Exception;
    public void persistSolSurtDataToLocalDb(List<TbSolicitadoSurtido> list) throws Exception;
    public List<TbSolicitadoSurtido> getTbSolicitadoSurtido1YearFromSendero();
    public void persistSolSurt1YearDataToLocalDb(List<TbSolicitadoSurtido> list) throws Exception;
    
}
