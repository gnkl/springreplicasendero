/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jmejia
 */
@Embeddable
public class TbSolicitadoSurtidoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "clapro")
    private String clapro;
    @Basic(optional = false)
    @Column(name = "fecent")
    @Temporal(TemporalType.DATE)
    private Date fecent;

    public TbSolicitadoSurtidoPK() {
    }

    public TbSolicitadoSurtidoPK(String clapro, Date fecent) {
        this.clapro = clapro;
        this.fecent = fecent;
    }

    public String getClapro() {
        return clapro;
    }

    public void setClapro(String clapro) {
        this.clapro = clapro;
    }

    public Date getFecent() {
        return fecent;
    }

    public void setFecent(Date fecent) {
        this.fecent = fecent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clapro != null ? clapro.hashCode() : 0);
        hash += (fecent != null ? fecent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSolicitadoSurtidoPK)) {
            return false;
        }
        TbSolicitadoSurtidoPK other = (TbSolicitadoSurtidoPK) object;
        if ((this.clapro == null && other.clapro != null) || (this.clapro != null && !this.clapro.equals(other.clapro))) {
            return false;
        }
        if ((this.fecent == null && other.fecent != null) || (this.fecent != null && !this.fecent.equals(other.fecent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.replica.model.TbSolicitadoSurtidoPK[ clapro=" + clapro + ", fecent=" + fecent + " ]";
    }
    
}
