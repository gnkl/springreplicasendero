/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.replica.component;

import java.util.List;
import mx.com.gnkl.replica.model.TbInventarioSendero;
import mx.com.gnkl.replica.repository.ReplicaDao;
import mx.com.gnkl.replica.repository.impl.ReplicaDaoImpl;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author jmejia
 */
@Component
public class SpringTemplateLermaTransaction extends ReplicaDaoImpl implements ReplicaDao{
    
        private TransactionTemplate transactionTemplate;
    
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}

	public void executeRep(final List<TbInventarioSendero> proceessList) throws Exception {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
                            System.out.println("is complete: "+status.isCompleted());
                            System.out.println("is new transaction: "+status.isNewTransaction());
                            System.out.println("is rool back: "+status.isRollbackOnly());
				persistDataToLocalDb(proceessList);
			}
		});
	}    
}
